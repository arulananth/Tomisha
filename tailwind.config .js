const colors = require('tailwindcss/colors');

module.exports = {
	theme: {
		extend: {
            colors: {
              'litepie-primary': colors.emerald
            }
          }
	},
	variants: {
		extend: {
			opacity: ["disabled"],
		},
	},
	plugins: [],
};